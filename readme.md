# Rubiko template html 

Este repositorio contiene un template de html, creado con [Boostrap](https://getbootstrap.com/),
[Font Awesome](https://fontawesome.com/) Y [VSC](https://code.visualstudio.com/).

## Primeros pasos

Estas instrucciones le proporcionarán una copia del proyecto en funcionamiento en su máquina local con fines de desarrollo y prueba. Consulte la implementación para obtener notas sobre cómo implementar el proyecto en un sistema en vivo.

### Prerrequisitos

Qué necesita para descargar el template e interactuar con el.


```
Visual Studio Code
Boostrap v4.5.2 
Font awesome
```
### Instalando

A continuación se muestra una serie de ejemplos paso a paso que le indican cómo ejecutar un entorno de desarrollo.

Paso 1.

```
Descargar Visual studio code e instalarlo en la computadora.
```

Paso 2.

```
Descargar la plantilla de este repositorio y abrilo con visual studio code.
```

Paso 3.

```
Editar el código a tu gusto.
```